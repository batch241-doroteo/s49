// Mock Database
// let posts = [];

// Post ID
// let count = 1;

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => showPosts(data))



// Reactive DOM with FETCH (CRUD Operation)

// ADD POST DATA
// This will trigger an event that will add a new post in our mock database upon clicking the "create" button.
document.querySelector("#form-add-post").addEventListener('submit', (e) => {
	// Prevents the default behavior of an event
	// To prevent the page from reloading
	e.preventDefault();
	// Adds an element in the end of an array and returns array's length

	// posts.push({
	// 	// "id" property will be used for the unique ID of each posts.
	// 	id: count,
	// 	// "title" and "body" values will come from the "form-add-post" input elements
	// 	title: document.querySelector('#txt-title').value,
	// 	body: document.querySelector('#txt-body').value
	// });
	// // count will increment everytime a new post is added
	// count++
	// console.log(posts);
	// alert("Post successfully added!");
	// showPosts();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}), 
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert("Post successfully added!");
		// to remove the text field information once create button was submmited
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})
});


// RETRIEVE POST
// (posts) - comming from the fetch link /posts
const showPosts = (posts) => {
	// variable that will contain all the posts
	let postEntries = "";
	// Looping through array items
	posts.forEach((post) => {
		// addition assignment (+=) operator adds the value of the right operand and assigns the result to the variable
		postEntries += `
		<div id="post-${post.id}">
		<h3 id="post-title-${post.id}">${post.title}</h3>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onClick="editPost('${post.id}')">Edit</button>
		<button onClick="deletePost('${post.id}')">Delete</button>
		</div>
		`
	});
	// To check what is stored in the postEntries variables
	console.log(postEntries);
	// To assign the value of postEntries to the element with "div-post-entries" id
	document.querySelector('#div-post-entries').innerHTML = postEntries
};

// EDIT POST (Edit Button)
const editPost = (id) => {

	// The function first uses the querySelector() method to get the element with the Id "#post-title-${id}" and "#post-body-${id}" and assigns its innerHTML property to the title variable with the same body
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	// removedAttribute() - removes the attribute with the specified name from the element
	document.querySelector('#btn-submit-update').removeAttribute('disabled')
	
};

// UPDATE POST (Update Button)

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault()

	// for(let i=0; i<posts.length; i++) {
	// 	if(posts[i].id.toString()=== document.querySelector('#txt-edit-id').value){
	// 		posts[i].title = document.querySelector('#txt-edit-title').value
	// 		posts[i].body = document.querySelector('#txt-edit-body').value

	// 		showPosts(posts)
	// 		alert('successfully updated')
	// 	}
	// }

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
	}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert('successfully updated')

		// to remove the text field information once create button was submmited
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		// setAttribute() -a adds/sets an attribute to an HTML element
		document.querySelector('#btn-submit-update').setAttribute('disabled', true)

	})

})

// ACTIVTY - DELETE POST

// const deletePost = (id) => {

// 	let postIndex = posts.findIndex(post => posts.id === posts.Id);

// 	if (postIndex !== -1) {
// 		posts.splice(postIndex, 1);
// 		showPosts(posts)
// 		alert('Post successfully removed!')
		
// 	}

// };

// ACTIVITY - DELETE POST (FETCH)

const deletePost = (id) => {

	fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
		method: 'DELETE',
		body: JSON.stringify({
			id: document.querySelector(`#post-${id}`),
			title: document.querySelector(`#post-title-${id}`),
			body: document.querySelector(`#post-body-${id}`),
			userId: 1
			}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert('successfully deleted')

	})

}





